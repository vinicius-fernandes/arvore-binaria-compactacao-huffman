/*
 * File:   main.cpp
 *
 * Created on 23 de Abril de 2018, 15:14
 */

#include <cstdlib>
#include <iostream>

#include "No.h"
#include "Arvore.h"

using namespace std;

int menu();

int main(int argc, char** argv)
{
    Arvore arvore;
    int continuar;
    int retornoBusca;
    No* no_removido;
    int opcao;
    int valor;
    No* ptAux;

    do
    {
        opcao = menu();

        switch (opcao)
        {
        case 1:
            cout << "\n Digite o valor para ser inserido: ";
            cin >> valor;
            arvore.Insere(valor, arvore.GetRaiz());
            cout << " Valor inserido com Sucesso";
            break;
        case 2:
            cout << "\n Digite o valor que deseja Buscar: ";
            cin >> valor;

            ptAux = arvore.GetRaiz();
            retornoBusca = arvore.Busca(valor, ptAux); // ptAux aponta p/ raiz da árvore

            if (retornoBusca == 1)
                cout << "\n O valor foi encontrado na arvóre ";
            else
                cout << "\n Valor não encontrado";
            break;
        case 3:
            cout << "\n Digite o valor que deseja Remover: ";
            cin >> valor;

            ptAux = arvore.GetRaiz();
            no_removido = arvore.Remove(valor, ptAux); // ptAux aponta p/ raiz da árvore

            if (no_removido == NULL)
                cout << "\n O valor não existe na Arvóre ";
            else
                cout << "\n Valor foi removido com sucesso";
            break;
        case 4:
            cout << "\n PreOrdem: ";
            arvore.PreOrdem(arvore.GetRaiz());
            cout << endl;
            break;
        case 5:
            cout << "\n EmOrdem: ";
            arvore.EmOrdem(arvore.GetRaiz());
            cout << endl;
            break;
        case 6:
            cout << "\n PosOrdem: ";
            arvore.PosOrdem(arvore.GetRaiz());
            cout << endl;
            break;
        case 7:
            cout << "\n EmNivel: ";
            arvore.EmNivel(arvore.GetRaiz());
            cout << endl;
            break;
        case 0:
            cout << "\n Encerrando aplicação ..." << endl;
            exit(0);
            break;
        default:
            cout << " \n Opção invalida";
            break;
        }

        cout << "\n\n Deseja continuar realizando operações na Arvore? (1 - Sim / 2 - Não): ";
        cin >> continuar;

        if (continuar == 2)
        {
            exit(0);
        }

    }
    while (opcao != 9);

    return 0;
}

int menu()
{
    int opcao = 0;

    system("clear");
    cout << "\n\n ================================================================" << endl;
    cout << "|  Trabalho 01 - Percursos em Árvores e Compactação de Dados     |" << endl;
    cout << " ================================================================" << endl;
    cout << "|" << "             1) Inserir                                         |" << endl;
    cout << "|" << "                2) Buscar                                       |" << endl;
    cout << "|" << "                   3) Remover                                   |" << endl;
    cout << "|" << "                      4) Pré-Ordem                              |" << endl;
    cout << "|" << "                         5) Em-Ordem                            |" << endl;
    cout << "|" << "                            6) Pós-Ordem                        |" << endl;
    cout << "|" << "                               7) Em-Nível                      |" << endl;
    cout << "|" << "                                 0) Sair                        |" << endl;
    cout << " ================================================================" << endl;
    cout << "\n O que deseja fazer? ";
    cin >> opcao;

    return opcao;
}
