/*
 * File:   Arvore.cpp
 *
 * Created on 23 de Abril de 2018, 17:48
 */

#include <deque>
#include <stddef.h>
#include <iostream>
#include <cstdlib>

#include "Arvore.h"

using namespace std;

Arvore::Arvore()
{
    raiz = NULL;
}

Arvore::Arvore(int chave)
{
    raiz = new No(chave);
}

Arvore::Arvore(const Arvore& orig)
{
}

Arvore::~Arvore()
{
    delete raiz;
}

void Arvore::SetRaiz(No* raiz)
{
    this->raiz = raiz;
}

No* Arvore::GetRaiz() const
{
    return raiz;
}

bool Arvore::Vazio()
{
    return (raiz == NULL);
}

int Arvore::Busca(int chave, No*& ptRaiz)
{
    int f; //Indica o resultado da busca: 0, 1, 2 ou 3

    if (ptRaiz == NULL)
        f = 0; // Árvore vazia
    else if (chave == ptRaiz->GetChave())
        f = 1; //Achou!
    else if (chave < ptRaiz->GetChave())   // Procurar do lado esquerdo
    {
        if (ptRaiz->GetEsq() == NULL)
            f = 2;
        else
        {
            ptRaiz = ptRaiz->GetEsq(); // Atualizando ponteiro: nova raiz!
            f = Busca(chave, ptRaiz);
        }
    }
    else     // Procurar do lado direito!
    {
        if (ptRaiz->GetDir() == NULL)
            f = 3;
        else
        {
            ptRaiz = ptRaiz->GetDir(); // Atualizando ponteiro: nova raiz!
            f = Busca(chave, ptRaiz);
        }
    }
    return f; // Resultado da busca
}

bool Arvore::Insere(int chave, No* ptRaiz)
{
    int f;
    No* pt;
    bool insOK = true;
    No* ptAux = ptRaiz; // Salvar raiz, ptAux vai ser modificado por Busca()

    f = Busca(chave, ptAux); // ptAux aponta p/ raiz da árvore

    if (f == 1) // Já está na árvore
        insOK = false;
    else   // Decidir de insere na subárvore: essq ou dir
    {
        pt = new No(chave); // Alocar novo nó da árvore

        if (f == 0)
            this->raiz = pt; // Nova raiz;
        else if (f == 2)
            ptAux->SetEsq(pt);
        else // f = 3
            ptAux->SetDir(pt);
    }
    return true;
}

No* Arvore::Remove(int chave, No*& raiz)
{
    if(raiz == NULL)
        return NULL;
    if(chave < raiz->GetChave())
    {
        No* esq = raiz->GetEsq();
        raiz->SetEsq(Remove(  chave, esq));
    }
    else if (chave > raiz->GetChave())
    {
        No* dir = raiz->GetDir();
        raiz->SetDir(Remove(  chave, dir));
    }
    else
    {
        if (raiz->GetEsq() == NULL && raiz->GetDir() == NULL)
        {
            delete raiz;
            raiz = NULL;
        }
        else if (raiz->GetEsq() == NULL)
        {
            No* temp = raiz;
            raiz = raiz->GetEsq();
            delete temp;
        }
        else
        {
            No *dir = raiz->GetDir();
            No *temp = LocalizaSucessor(raiz->GetDir());
            raiz->SetChave(temp->GetChave());
            raiz->SetDir(Remove(temp->GetChave(), dir));
        }
    }
    return raiz;
}

No* Arvore::LocalizaSucessor(No* pt)
{
    No* ptAux = pt;

    if (pt->GetDir() != NULL)
        LocalizaSucessor(pt->GetDir());

    return ptAux;
}

void Arvore::PreOrdem(No* raiz)
{
    if (!Vazio())
    {
        Visita(raiz);

        if (raiz->GetEsq() != NULL)
            PreOrdem(raiz->GetEsq());

        if (raiz->GetDir() != NULL)
            PreOrdem(raiz->GetDir());
    }
}

void Arvore::EmOrdem(No* raiz)
{
    if (!Vazio())
    {
        if (raiz->GetEsq() != NULL)
            EmOrdem(raiz->GetEsq());

        Visita(raiz);

        if (raiz->GetDir() != NULL)
            EmOrdem(raiz->GetDir());
    }
}

void Arvore::PosOrdem(No* raiz)
{
    if (!Vazio())
    {
        if (raiz->GetEsq() != NULL)
            PosOrdem(raiz->GetEsq());

        if (raiz->GetDir() != NULL)
            PosOrdem(raiz->GetDir());

        Visita(raiz);
    }
}

void Arvore::EmNivel(No* raiz)
{
    if (!Vazio())
    {
        deque<No*> fila;

        fila.push_back(raiz);
        while (!fila.empty())
        {
            No* no_atual = fila.front();
            fila.pop_front();
            Visita(no_atual);

            if (no_atual->GetEsq() != NULL)
                fila.push_back(no_atual->GetEsq());

            if (no_atual->GetDir() != NULL)
                fila.push_back(no_atual->GetDir());
        }
    }
}

void Arvore::Visita(No* raiz)
{
    cout << raiz->GetChave() << ", ";
}

